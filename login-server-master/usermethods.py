from distutils.command import check
from flask import request
from app import app, session,  conn 
from json import dumps, loads
from flask_login import login_required
import datetime
from apsw import Error 
from markupsafe import escape

#Method only allows to search messages where the current user is present
#Currently injection proof. 
@app.route('/search', methods=['POST', 'GET'])
@login_required
def search():  
    username = session["user"]
    searchedUser = request.args.get("searchedUser") or request.form.get("searchedUser")
    try:
        if searchedUser == "*":
            c = conn.execute('SELECT * FROM messages WHERE sender = ? OR recipient = ?', (username,username))
        elif searchedUser in userlist():
            c = conn.execute('SELECT * FROM messages WHERE sender = ? AND recipient = ? OR sender = ? AND recipient = ?', (username,searchedUser,searchedUser,username))
        else:
            return f"User: {searchedUser} doesn't exist, please try again."
        rows = c.fetchall()
        result = 'Messages sent and received [ID : USER : MESSAGE : RECIPIENT : TIME]:\n'
        for row in rows:
            result = f'{result}    {dumps(row)}\n'
        c.close()
        return result   
    except:
        return 'Error, did you remember to put in recipient?'

##Function for admin to view all messages
@app.route('/searchAll', methods=['POST', 'GET'])
@login_required
def search_all():
    username = session["user"]
    try:
        c = conn.execute('SELECT role FROM users WHERE username = ?', (username,))
        role = c.fetchone()[0]
        if role != "admin":
            return "Unable to show all messages, admins only!"
        c.close()
        show_all = conn.execute('SELECT * FROM messages')
        all_msg = show_all.fetchall()
        result = 'Messages sent and received [ID : USER : MESSAGE : RECIPIENT : TIME]:\n'
        for message in all_msg:
            result = f'{result}    {dumps(message)}\n'
        show_all.close()
        return result 
    except Error as e:
        return (f'{result}ERROR: {e}', 500)

#Method store message in table with current user, message, reciever and date. 
#Currently injection proof. 
@app.route('/send', methods=['POST','GET'])
@login_required
def send():
    blocked_str = ""
    username = session["user"]
    date_now = datetime.datetime.today().isoformat()
    users = userlist()
    blocked_users = set() 
    fake_users = set()  #compare fake_users and users put in 
    try: 
        message = request.args.get('message') or request.args.get('message')
        recipient = request.args.get('recipient') or request.form.get('recipient')
        rec_list = recipient.split(",")

        ##Checks for username when it is only one in input
        if len(rec_list) == 1 and not recipient in users:
            return f'ERROR! Username does noe exist!'

        if not username or not message:
            return f'ERROR! You need to add something to the message!'

        for person in rec_list:
            if not person in users:
                fake_users.add(person)
                continue
            elif check_if_can_send(person, username) or check_if_can_send(username, person):
                blocked_str += f'{person} is blocked. You cannot send message to them.\n'
                blocked_users.add(person) 
                continue 
            else:
                conn.execute('INSERT INTO messages (sender, message, recipient, time) VALUES (?,?,?,?)', (username,message,person, date_now))
        
        #Set of real users 
        recipient = set(rec_list) - fake_users

        #Return if no provided usernames are valid 
        if len(recipient) == 0:
            return f'ERROR! No valid usernames!'
        
        #Set of actual receivers
        recipient -= blocked_users
        recipient = ", ".join(recipient)
        if len(recipient) == 0:
            return f'{blocked_str}'
        else: 
            return f'{username} says: {message}\nto: {recipient},\n{blocked_str}'
    except:
        return 'Error, did you remember to put in recipient?'

##Ensures that you cannot send to someone who has blocked the current user: 
def check_if_can_send(user1:str, user2:str):
    c = conn.execute('SELECT blocked FROM users WHERE username = ?',(user1,))
    dict_json = dumps(c.fetchone())[1:-1]
    dict = eval(loads(dict_json))
    list_of_names = dict
    c.close() 
    for name in list_of_names:
        if user2 == name:
            return True
    return False 



#Adds the name of the user to block to the user table in our database 
@app.route('/block', methods=['POST', 'GET'])
@login_required
def block():
    uname = session["user"]
    try:
        user = request.args.get("blockUser") or request.form.get("blockUser")
    
        if not uname or not user:
            return f'ERROR: missing user'
        c = conn.execute(f"SELECT blocked FROM users WHERE username = ?", (uname,))
        dict_json = dumps(c.fetchone())[1:-1]
        dict = eval(loads(dict_json))
        dict.append(user)
        new_dict_json = dumps(dict)
        conn.execute('UPDATE users SET blocked = ? WHERE username = ?', (new_dict_json, uname))
        c.close()
        return f'You blocked {user}. They are not allowed to send you messages anymore. Thank the lord!'
    except Error as e:
        return f'ERROR: {e}'

##Unblocks all user for current user 
@app.route('/unblock', methods=['POST', 'GET'])
@login_required
def unblock():
    uname = session["user"]
    try: 
        c = conn.execute(f"SELECT blocked FROM users WHERE username = ?", (uname,))
        dict_json = dumps(c.fetchone())[1:-1]
        dict = eval(loads(dict_json))
        dict = []
        update_dict = dumps(dict)
        conn.execute('UPDATE users SET blocked = ? WHERE username = ?', (update_dict, uname))
        c.close()
        return f'You unblocked all blocked users' 
    except Error as e:
        return f'ERROR: {e}'

@app.get('/announcements')
@login_required
def announcements():
    try:
        stmt = f"SELECT author,text FROM announcements;"
        c = conn.execute(stmt)
        anns = []
        for row in c:
            anns.append({'sender':escape(row[0]), 'message':escape(row[1])})
        c.close()
        return {'data':anns}
    except Error as e:
        return {'error': f'{e}'}

##Retrieves the list of users 
def userlist():
    stmt = f"SELECT username FROM users;"
    c = conn.execute(stmt) 
    users = str(dumps(c.fetchall())).split(",")
    userlist = []
    for user in users:
        user = user.replace("[", "").replace("]", "").replace('"', "").replace(" ", "")
        userlist.append(user)
    c.close()
    return userlist
