from flask import Flask, request, send_from_directory, render_template, url_for, redirect, session
from flask_login import logout_user
import flask
from forms import LoginForm, RegistrationForm
from json import dumps
import sys
import apsw
from apsw import Error
from threading import local
from urllib.parse import urlparse, urljoin
import bcrypt
import flask_login
from flask_login import login_required, login_user
from pygments.formatters import HtmlFormatter


cssData = HtmlFormatter(nowrap=True).get_style_defs('.highlight')

tls = local()
    
# Set up app
app = Flask(__name__)
# The secret key enables storing encrypted session data in a cookie (session)
app.secret_key = '192b9bdd22ab9ed4d12e236c78afcb9a393ec15f71bbf5dc987d54727823bcbf'
# The admin password hashed to compare when an admin registers with admin password.
admin_hashed_password = b'$2b$12$sBHLuCrBlMkEJtiCkw63R.Hdl6MPw/SVDjkqc1XBBRrgYECi20Pei'
# A list of roles
role_list = ["admin", "user"]

# Add a login manager to the app
login_manager = flask_login.LoginManager()
login_manager.init_app(app)
login_manager.login_view = "login"

# Class to store user info
class User(flask_login.UserMixin):
    pass

# This method is called whenever the login manager needs to get
# the User object for a given user id
@login_manager.user_loader
def user_loader(user_id):
    user = User()
    user.id = user_id
    return user

@app.route('/goto_registration')
def user_reg():
    return flask.redirect(url_for('register'))

##Register user if user does not exist 
@app.route('/register', methods = ['POST','GET']) 
def register():
    form = RegistrationForm()
    if form.validate_on_submit():
        uname = form.username.data
        passwrd = form.password.data.encode('utf-8')
        role = form.role.data.lower()
        if role not in role_list:
            flask.flash("Incorrect role, please specify admin or user!", "RegistrationError")
            return render_template('registration.html', form=form)
        if role == "admin":
            admin_passwrd = form.adminPassword.data.encode('utf-8')
            if not bcrypt.checkpw(admin_passwrd, admin_hashed_password):
                flask.flash("Admin password is not correct!", "RegistrationError")
                return render_template('registration.html', form=form)
        ulist = userlist()
        if uname in ulist: 
            flask.flash("Username taken, please try again with a different username", "RegistrationError")
            return render_template('registration.html', form=form)
        hashed_passwrd = bcrypt.hashpw(passwrd, bcrypt.gensalt())
        blocked_init = []
        blocked_json = dumps(blocked_init)
        conn.execute('INSERT INTO users (username, password, blocked, role) VALUES (?,?,?,?)', (uname, hashed_passwrd.decode(), blocked_json, role))
        return redirect(url_for('login'))
    return render_template('registration.html', form=form) 
    

@app.route('/')
@app.route('/index.html')
@login_required
def index_html():
    return send_from_directory(app.root_path,
                        'index.html', mimetype='text/html')

##Authenticates and logs in user 
@app.route('/')
@app.route('/login', methods=['GET', 'POST'])
def login():
    form = LoginForm()
    if form.validate_on_submit():
        username = form.username.data
        password = form.password.data.encode('utf-8') # comparing passwords with bcrypt: https://openbase.com/js/bcrypt/documentation
        try:
            c = conn.execute('SELECT password FROM users WHERE username = ?', (username,)) 
            passwrd = dumps(c.fetchone())
            passwrd2 = passwrd[2:len(passwrd) - 2]
            if bcrypt.checkpw(password, passwrd2.encode('utf-8')):
                user = user_loader(username)
                # automatically sets logged in session cookie
                login_user(user)
                session["user"] = username
            else:
                raise
        except:
            c.close() 
            flask.flash('Username/passwords did not match', "LoginError")
            return render_template('login.html', form=form)
        c.close()
        return flask.redirect(flask.url_for('index_html'))

    return render_template('./login.html', form=form)

##Logs out user 
@app.route('/logout')
@login_required
def logout():
    logout_user()
    session.pop("user", None)
    return flask.redirect(url_for('login'))

#lagt til for å få safe_url til å funk.
#taken from: https://stackoverflow.com/questions/60532973/how-do-i-get-a-is-safe-url-function-to-use-with-flask-and-how-does-it-work
def is_safe_url(target):
    ref_url = urlparse(request.host_url)
    test_url = urlparse(urljoin(request.host_url, target))
    return test_url.scheme in ('http', 'https') and \
           ref_url.netloc == test_url.netloc

##Create tables in database 
try:
        conn = apsw.Connection('./tiny.db')
        c = conn.cursor()
        c.execute('''CREATE TABLE IF NOT EXISTS messages (
            id integer PRIMARY KEY, 
            sender TEXT NOT NULL,
            message TEXT NOT NULL,
            recipient TEXT NOT NULL,
            time TEXT NOT NULL);''')
        c.execute('''CREATE TABLE IF NOT EXISTS announcements (
            id integer PRIMARY KEY, 
            author TEXT NOT NULL,
            text TEXT NOT NULL);''')
        c.execute('''CREATE TABLE IF NOT EXISTS users (
            id integer PRIMARY KEY,
            username TEXT NOT NULL,
            password TEXT NOT NULL,
            blocked TEXT NOT NULL,
            role TEXT NOT NULL);''')
except Error as e:
        print(e)
        sys.exit(1)

  
from usermethods import *